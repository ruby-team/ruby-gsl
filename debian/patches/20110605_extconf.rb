Description: install in $vendorarchdir according to Debian Ruby Policy
Origin: vendor
Last-Update: 2016-10-30

--- a/ext/gsl_native/extconf.rb
+++ b/ext/gsl_native/extconf.rb
@@ -52,7 +52,7 @@
 end
 
 def gsl_dir_config(target, idir = nil, ldir = idir)
-  dir_config(target, idir || $sitearchdir, ldir || $sitearchdir)
+  dir_config(target, idir || $vendorarchdir, ldir || $vendorarchdir)
 end
 
 def gsl_gem_config(target, dir = 'ext')
