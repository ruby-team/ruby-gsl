ruby-gsl (2.1.0.3+dfsg1-6) UNRELEASED; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.

  [ Cédric Boutillier ]
  * Add gsl2.8.patch to fix compatibility with GSL2.8 (Closes: #1076659)
    Thanks Yavor Doganov for the report and the patch
  * clean .ccls-cache/

  [ Youhei SASAKI ]
  * Add patch: NArray support, gcc-14 incompatible-pointer-types (Closes: #1075462)
  * Remove X?-Ruby-Versions fields from d/control

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Mon, 30 Dec 2024 19:34:12 +0900

ruby-gsl (2.1.0.3+dfsg1-5) unstable; urgency=medium

  * Team upload.
  * Re-upload for gsl and Ruby 3.0 transition.
  * d/README.source: Remove inapplicable paragraph.
  * d/copyright (Copyright): Update.
  * d/watch: Modernize file.

 -- Daniel Leidert <dleidert@debian.org>  Fri, 03 Dec 2021 19:08:42 +0100

ruby-gsl (2.1.0.3+dfsg1-4) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Drop transition for old debug package migration.
  * Update standards version to 4.5.1, no changes needed.

  [ Lucas Kanashiro ]
  * Add patch to add ruby3.0 compatibility (Closes: #996237)
  * Remove unused patches
  * Declare compliance with Debian Policy 4.6.0

 -- Lucas Kanashiro <kanashiro@debian.org>  Thu, 04 Nov 2021 16:45:12 -0300

ruby-gsl (2.1.0.3+dfsg1-3) unstable; urgency=medium

  * Team upload.

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.4.1, no changes needed.

  [ Daniel Leidert ]
  * d/control: Add Testsuite and Rules-Requires-Root fields.
    (Maintainer): Fix team name.
    (Standards-Version): Bump to 4.5.0.
    (Build-Depends): Remove quilt.
    (Depends): Use ${ruby:Depends} and remove interpreters.
  * d/copyright: Reformat and re-order.
    (Upstream-Contact, Source): Fix URL and use the SciRuby repositories.
    (Copyright): Add the team.
  * d/rb_gsl_config.h: Remove unused file.
  * d/ruby-gsl.docs: Add file to install README.md.
  * d/ruby-tests.rake: Add test file and use it over d/ruby-tests.rb.
  * d/ruby-tests.rb: Remove file.
  * d/rules: Use installation layout and check dependencies.
    (override_dh_installdocs): Use dh_installexamples instead.
  * d/upstream/metadata: Add Archive, Changelog, and Repository* fields.

 -- Daniel Leidert <dleidert@debian.org>  Sat, 24 Oct 2020 18:59:22 +0200

ruby-gsl (2.1.0.3+dfsg1-2) unstable; urgency=medium

  * Add skip_failing_TDS_cyc_solve.patch, skipping failing test on i386
    (Closes: #909863)
  * Bump Standards-Version to 4.3.0 (no changes needed)
  * Use salsa.debian.org in Vcs-* fields
  * Move debian/watch to gemwatch.debian.net
  * Bump debhelper compatibility level to 11

 -- Cédric Boutillier <boutil@debian.org>  Sat, 29 Dec 2018 01:21:44 +0100

ruby-gsl (2.1.0.3+dfsg1-1) unstable; urgency=medium

  [ Christian Hofstaedtler ]
  * Remove Daigo Moriwaki from Uploaders: list per MIA request.
    Thank you for your work! (Closes: #863774)

  [ Balint Reczey ]
  * New upstream version 2.1.0.3+dfsg1 (Closes: #889023)
  * Use my @ubuntu.com email address in Maintainer field
  * Migrate to automatic generated debug symbol package from ruby-gsl-dbg
  * Fix FTBFS due to failing interp2d_test and spline2d_test

 -- Balint Reczey <rbalint@ubuntu.com>  Mon, 12 Feb 2018 23:45:10 +0700

ruby-gsl (2.1.0.1+dfsg1-1) unstable; urgency=medium

  * Remove tmp/ dir from original tarball
  * Imported Upstream version 2.1.0.1+dfsg1
  * Refresh patches.
    Skip test_nonsymm test which is failing on some architectures due
    to calculation precision

 -- Balint Reczey <balint@balintreczey.hu>  Tue, 27 Dec 2016 01:45:20 +0100

ruby-gsl (2.1.0+dfsg1-1) unstable; urgency=medium

  [ Cédric Boutillier ]
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Use https:// in Vcs-* fields
  * Run wrap-and-sort on packaging files
  * Imported Upstream version 2.1.0+dfsg1

  [ Balint Reczey ]
  * Bump standards version to 3.9.8

 -- Balint Reczey <balint@balintreczey.hu>  Mon, 26 Dec 2016 11:20:25 +0100

ruby-gsl (1.16.0.6+dfsg1-2) unstable; urgency=medium

  * Skip dht tests, failing on several arches due to precision issues.

 -- Cédric Boutillier <boutil@debian.org>  Tue, 01 Mar 2016 15:35:10 +0100

ruby-gsl (1.16.0.6+dfsg1-1) unstable; urgency=medium

  [ Balint Reczey ]
  * Update watch file to use gsl as gem name
  * Imported Upstream version 1.16.0.6+dfsg1

  [ Cédric Boutillier ]
  * add patches gsl2_* patches to build with GSL 2.x (Closes: #804499)
    - fix build failures (Closes: #815768, #798535)
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Set the Debian Ruby team as the maintainer of the package
  * Do not exclude histo_test.rb in tests anymore

 -- Cédric Boutillier <boutil@debian.org>  Mon, 29 Feb 2016 15:15:25 +0100

ruby-gsl (1.16.0.4+dfsg1-1) unstable; urgency=medium

  * Remove rdoc/ during uscan download
  * Switch to active upstream fork at github.com/blackwinter
  * Add myself to Uploaders
  * New upstream release (Closes: #761169)
  * Refresh patches
  * Simplify d/rules
  * Bump standards version
  * Run tests skipping a failing file

 -- Balint Reczey <balint@balintreczey.hu>  Mon, 08 Jun 2015 15:54:29 +0200

ruby-gsl (1.15.3+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Port to Ruby 2.1 (Closes: #744805)

 -- Antonio Terceiro <terceiro@debian.org>  Thu, 17 Apr 2014 17:44:49 -0300

ruby-gsl (1.15.3+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 1.15.3
  * debian/watch: use gemwatch to track gsl gem
  * add ruby-tests.rb to run tests at build time
  * debian/control:
    - add myself to Uploaders
    - bump Standards-Version to 3.9.5 (no changes needed)
    - drop transitional packages
    - replace ruby1.8 by ruby in Depends
    - normalise VCS-Git url
  * debian/patches:
    - refresh patches 20110605_extconf.rb and 20130815_deprecated_enum.patch
    - deactivate 0110605_*_complex.c patches; not needed anymore
    - add 20131016_fix_test_typo.patch: fix typo in a test
    - add 20131016_test_output_less_verbose.patch: make tests less verbose
      when passing
    - add 20131016_deactivate_failing_tests.patch: skip failing tests on amd64
    - add DEP-3 headers to 00{1,2}*.patch files
  * debian/rules: drop --with quilt switch
  * Repack source to remove non-free documentation (Closes: #709497)
    - add debian/gbp.conf to filter out rdoc directory, containing files under
      the GNU Documentation license with invariant sections.
    - do not build the doc in debian/rules
    - add dversionmangle option in debian/watch to strip +dfsg suffix
    - document in debian/README.source how the repack is handled
  * debian/copyright: port to copyright-format/1.0

 -- Cédric Boutillier <boutil@debian.org>  Sat, 30 Nov 2013 17:26:48 +0100

ruby-gsl (1.14.7-2) unstable; urgency=low

  * Team upload.
  * Added patch to rename deprecated enum. (Closes: #718070)

 -- Guillaume Bouteille <duffman@gb2n.org>  Thu, 15 Aug 2013 17:14:28 +0200

ruby-gsl (1.14.7-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Moved Debian repository from svn to git.
    - Bumped up Standards-Version to 3.9.2.
    - Followed the NArray package name change from libnarray-ruby to
      ruby-narray.
    - Build-Depends libtamuanova-dev to build a related module.
  * debian/rules: Clean auto-generated files (Closes: #628264)
  * Added patches to work around build failure:
    - debian/patches/20110605_matrix_complex.c
    - debian/patches/20110605_vector_complex.c
  * debian/patches/20110605_extconf.rb: Added a patch to find narra.h
    correctly.
  * Converted the build system to gem2deb, removed build dependency on cdbs
    and ruby-pkg-tools

 -- Daigo Moriwaki <daigo@debian.org>  Sun, 05 Jun 2011 22:53:59 +0900

libgsl-ruby (1.14.3-1) unstable; urgency=low

  [ Deepak Tripathi ]
  * New upstream release
  * Switch to dpkg-source 3.0 (quilt) format
      + added debian/source
  * Bumped Standard Version to 3.8.4
  * debian/control
      + added myself to uploaders
      + ported to ruby1.9.1
  * debian/patches
      + added patch for bashisum.
      + added patch for min.c's GSL_1_2_LATER checking.
  * debian/
      + added README.source
  * debian/copyright
      + updated copyright file.

  [ Daigo Moriwaki ]
  * debian/control: Provides -dbg packages.
  * Removed debian/libgsl-ruby-doc.docs, debian/libgsl-ruby1.8.docs:
    - Merged to debian/rules.

 -- Daigo Moriwaki <daigo@debian.org>  Sat, 15 May 2010 19:25:22 +0900

libgsl-ruby (1.10.3-3) unstable; urgency=low

  * Added debian/patches/20091022-DashAsBinSh-1.10.3.patch:
    Fixed "bashism in /bin/sh script" (Closes: #530117)

 -- Daigo Moriwaki <daigo@debian.org>  Fri, 23 Oct 2009 20:18:32 +0900

libgsl-ruby (1.10.3-2) unstable; urgency=low

  [ Daigo Moriwaki ]
  * Applied a patch from Daniel Moerner
    + debian/control:
      - Bumped up Standards-Version to 3.8.3.
      - Added misc:Depends correctly.
    + debian/copyright:
      - Added years of the copyright holder.
      - Updated a pointer to the LGPL file.
  * debian/patches/20091021-GSL_CONST_CGSM_GAUSS-removed-from-upstream-
    libgsl-1.3.patch:
    + Thanks to Daniel Moerner
    + Fixed a FTBFS error. (Closes: #549435)

  [ Gunnar Wolf ]
  * Changed section to Ruby as per ftp-masters' request

 -- Daigo Moriwaki <daigo@debian.org>  Fri, 23 Oct 2009 07:41:43 +0900

libgsl-ruby (1.10.3-1) unstable; urgency=low

  [ Daigo Moriwaki ]
  * New upstream release.

  [ Lucas Nussbaum ]
  * Use new Homepage dpkg header.

 -- Daigo Moriwaki <daigo@debian.org>  Fri, 21 Dec 2007 12:54:52 +0900

libgsl-ruby (1.9.2-3) unstable; urgency=low

  * Fixed FTBFS: min.c:128: error: static declaration of
    'gsl_min_fminimizer_x_minimum' follows non-static
    declaration (Closes: #443013)
    - Added debian/patches/min.c.patch
    - debian/rules now uses simple-patchsys.mk
  * debian/control: an extra space is required before 'homepage'.

 -- Daigo Moriwaki <daigo@debian.org>  Sat, 22 Sep 2007 20:38:59 +0900

libgsl-ruby (1.9.2-2) unstable; urgency=low

  * The bug, libgsl-ruby1.8: Strange behaviour when accessing GSL::Vector
    elements with get() and [], has been fixed by the upstream's new
    releaase (Closes: #423850).

 -- Daigo Moriwaki <daigo@debian.org>  Sun, 02 Sep 2007 11:56:58 +0900

libgsl-ruby (1.9.2-1) unstable; urgency=low

  * New upstream release.

 -- Daigo Moriwaki <daigo@debian.org>  Sun, 02 Sep 2007 11:39:20 +0900

libgsl-ruby (1.9.0-4) unstable; urgency=low

  * debian/control: use shlibs instead of hardconding the dependencies.

 -- Daigo Moriwaki <daigo@debian.org>  Sun, 02 Sep 2007 11:32:30 +0900

libgsl-ruby (1.9.0-3) unstable; urgency=low

  * Fixed dependancy: libgsl0 -> libgsl0ldbl.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Mon, 27 Aug 2007 21:23:51 +0200

libgsl-ruby (1.9.0-2) unstable; urgency=low

  * A library, rb_gsl.so,  was errornously installed into
    /usr/local/lib/site_ruby/1.8/... Now it will go to
    /usr/lib/site_ruby/1.8/...

 -- Daigo Moriwaki <daigo@debian.org>  Sun, 13 May 2007 14:17:44 +0900

libgsl-ruby (1.9.0-1) unstable; urgency=low

  * New upstream release.
  * The package has been taken over. I am now a primary maintainer.
  * Built with NArray to support NArray compatibilities. Now it depends on
    libnarray-ruby1.8.
  * Stopped using the "Uploaders rule".

 -- Daigo Moriwaki <daigo@debian.org>  Mon, 16 Apr 2007 22:28:33 +0900

libgsl-ruby (1.8.3-1) unstable; urgency=low

  * Initial release.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Tue,  6 Jun 2006 21:09:15 +0200
